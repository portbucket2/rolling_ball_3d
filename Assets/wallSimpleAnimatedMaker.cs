﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSimpleAnimatedMaker : MonoBehaviour {

    public float size;
    public GameObject wall;
    public float maxSize;

    // Use this for initialization
    void Start () {
        size = Random.Range(5, maxSize);
        wall.transform.localScale = new Vector3(size, wall.transform.localScale.y, wall.transform.localScale.z);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

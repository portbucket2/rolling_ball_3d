﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSideMaker : MonoBehaviour {

    public float[] posX;
    public float size;
    public float maxSize;
    public GameObject wallHolder;
    

	// Use this for initialization
	void Start () {
        
        size = Random.Range(8, maxSize);
        posX[0] = 10.5f - (size/2);
        posX[1] = -(10.5f - (size / 2));
        int i = Random.Range(0, 2);


        
        wallHolder.transform.localScale = new Vector3(size,1,1);
        transform.position = new Vector3(posX[i],0,transform.position.z);



    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

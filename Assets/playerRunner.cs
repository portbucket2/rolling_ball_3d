﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playerRunner : MonoBehaviour {

    public GameObject gameManager;
    public GameObject playerHolder;
    public float speed;
    public float rotSpeed;
    public float limit;
    public GameObject ball;
    public GameObject ballHolder;
    public GameObject ballHolder2;
    public ParticleSystem[] sideParticles;
    public ParticleSystem ballExplodeParticle;
    public ParticleSystem coinGainParticle;
    public int coins;
    public Text coinsText;
    public float levelPercentage;
    public Text levelPercentageText;
    float currentSpeed;

    private Rigidbody rb;

    public bool dead = false;



    //public ParticleSystem 


    public int x, z, xApl;


    // Use this for initialization
    void Start()
    {

        x = 1;
        z = 0;
        xApl = x;
        coinsText.text = coins + "";
    }

    // Update is called once per frame
    void Update()
    {
        //rb.velocity = new Vector3(0, 0, currentSpeed * z * 2 * 50f);
        if (gameManager.GetComponent<gameManagerr>().playing == true || gameManager.GetComponent<gameManagerr>().levelEnd == true)
        {
            if (gameManager.GetComponent<gameManagerr>().playing == true)
            {
                
                TurnAltNew();
            }
            else if (gameManager.GetComponent<gameManagerr>().levelEnd == true)
            {
                xApl = x;
                z = 0;
                currentSpeed = speed * 0.2f;
                
            }
            MovePlayer();
            UpdateLevelPercentage();

            //TurnAlt();
            
            //this.transform.Translate(speed * xApl, 0, speed * z * 2);
            //playerHolder.transform.Translate(speed * xApl, 0, currentSpeed * z * 2);

            rb = this.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(speed * xApl *50f, 0, currentSpeed * z * 2 * 50f);
            

            //this.transform.position += new Vector3(speed * xApl, 0, speed * z * 2);
            //ballHolder.transform.Rotate(z * 100f, 0, 0);
            //ball.transform.rotation = ball.transform.rotation * Quaternion.Euler(z * 10, 0, 0);
            ballHolder2.transform.rotation = ballHolder2.transform.rotation * Quaternion.Euler(0, 0, -xApl * rotSpeed);
            ballHolder.transform.rotation = ballHolder.transform.rotation * Quaternion.Euler(z * 2* rotSpeed, 0, 0);

            if (Input.GetMouseButton(0))
            {
                ball.transform.SetParent(ballHolder.transform);
                //ballHolder2.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                ball.transform.SetParent(ballHolder2.transform);
                //ballHolder.transform.localRotation = Quaternion.Euler(0, 0,0);
            }
        }
        
    
    
    }
    
    //private void FixedUpdate()
    //{
    //    //this.transform.Translate(speed * xApl, 0, speed * z * 2);
    //    if(gameManager.GetComponent<gameManagerr>().playing == true)
    //    {
    //        ballHolder2.transform.rotation = ballHolder2.transform.rotation * Quaternion.Euler(0, 0, -xApl * rotSpeed);
    //        ballHolder.transform.rotation = ballHolder.transform.rotation * Quaternion.Euler(z * 2 * rotSpeed, 0, 0);
    //
    //        
    //    }
    //    
    //
    //}

    void MovePlayer()
    {

        if ( this.transform.position.x < -limit )
        {

            x = 1;
            //z *= -1;
            sideParticles[0].Play();
        }
        else if (this.transform.position.x > limit)
        {

            x = -1;
            //z *= -1;
            sideParticles[1].Play();
        }


    }

    //void Turn()
    //{
    //    if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
    //    {
    //        if(turned == false)
    //        {
    //            turned = true;
    //        }
    //        else if (turned == true)
    //        {
    //            turned = false;
    //        }
    //    }
    //    if(turned)
    //    {
    //        x = 1;
    //        z = 0;
    //    }
    //    if (!turned)
    //    {
    //        x = 0;
    //        z = 1;
    //    }
    //}

    void TurnAlt()
    {
        Vector2 mousePos = Input.mousePosition;
        if (Input.GetKeyDown(KeyCode.D) || (Input.GetMouseButtonDown(0) && (mousePos.x > Screen.width / 2)))
        {
            if (x == 1 || x == -1)
            {
                x = 0;
                z = -1;
            }
            else if (z == 1 || z == -1)
            {
                x = 1;
                z = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.A) || (Input.GetMouseButtonDown(0) && (mousePos.x < Screen.width / 2)))
        {
            if (x == 1 || x == -1)
            {
                x = 0;
                z = 1;
            }
            else if (z == 1 || z == -1)
            {
                x = -1;
                z = 0;
            }
        }

    }
    void TurnAltNew()
    {
        Vector2 mousePos = Input.mousePosition;
        if (Input.GetMouseButton(0) )
        {
            
            xApl = 0;
            z = 1;
            //ballHolder.transform.localRotation = Quaternion.Euler(0, 0, 0);
            currentSpeed = Mathf.Lerp(currentSpeed, speed, Time.deltaTime * 5);
            
        }
        else
        {
            xApl = x;
            z = 0;
            currentSpeed = speed * 0.2f;
        }
        //if (Input.GetMouseButtonUp(0))
        //{
        //    if(transform.position.x > 0)
        //    {
        //        x = -1;
        //    }
        //    else if (transform.position.x < 0)
        //    {
        //        x = 1;
        //    }
        //    z = 0;
        //}
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "wall")
        {
            //Destroy(this.gameObject);
            speed = 0;
            dead = true;
            sideParticles[0].Play();
            sideParticles[1].Play();
            Invoke("Restart", 1.5f);
            ExplodeBall();
        }
        if (other.tag == "coin" && other.gameObject.GetComponent<detectPlayer>().breakCoin == false)
        {
            Destroy(other.gameObject);
            UpdateCoins();
            coinGainParticle.Play();
            gameManager.GetComponent<gameManagerr>().UpdateCoinsTotal();

        }
        if (other.tag == "star")
        {
            Destroy(other.gameObject);
            gameManager.GetComponent<gameManagerr>().UpdateStarsCurrent();
            gameManager.GetComponent<gameManagerr>().UpdateStarsTotal();

        }
        if (other.tag == "finishline")
        {
            //gameManager.GetComponent<gameManagerr>().NextLevel();
            gameManager.GetComponent<gameManagerr>().LevelEnded();
            gameManager.GetComponent<gameManagerr>().LevelPassing();
            //Invoke("Restart", 0.2f);

        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
    public void Pause()
    {
        Time.timeScale = 0;
    }
    public void Resume()
    {
        Time.timeScale = 1;
    }

    public void UpdateCoins()
    {
        coins += 1;
        coinsText.text = coins + "";
        gameManager.GetComponent<gameManagerr>().UpdateCoinsCurrent();
    }
    public void UpdateLevelPercentage()
    {
        
        levelPercentage = Mathf.Round(transform.position.z * 100 / 520);
        levelPercentage = Mathf.Clamp(levelPercentage, 0, 100);

        levelPercentageText.text = levelPercentage + "%";
        
    }

    public void ExplodeBall()
    {
        ballExplodeParticle.Play();
        ball.SetActive(false);
    }
}




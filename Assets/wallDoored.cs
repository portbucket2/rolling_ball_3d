﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallDoored : MonoBehaviour {

    public float[] posX;
    public int size;
    public GameObject[] wall;
    public float doorPos;
    public float halfWidth;


    // Use this for initialization
    void Start () {
        
        size = Random.Range(5, 10);
        doorPos = Random.Range((-(size/2)+1), ((size / 2) - 1));
        posX[0] = ((doorPos + (size / 2)) + halfWidth)/2;
        posX[1] = ((doorPos - (size / 2)) - halfWidth) / 2;
        


        
        wall[0].transform.localScale = new Vector3((halfWidth - posX[0])*2, wall[0].transform.localScale.y, 1);
        wall[1].transform.localScale = new Vector3((halfWidth + posX[1])*2, wall[0].transform.localScale.y, 1);
        wall[0].transform.position = new Vector3(posX[0],0,transform.position.z);
        wall[1].transform.position = new Vector3(posX[1], 0, transform.position.z);



    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camClosing : MonoBehaviour {

    public GameObject gameManager;
    public Vector3 closePos;
    public Vector3 farPos;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(gameManager.GetComponent<gameManagerr>().menu == true)
        {
            this.transform.localPosition = Vector3.Slerp(this.transform.localPosition, closePos, Time.deltaTime*3);
        }
        else
        {
            this.transform.localPosition = Vector3.Slerp(this.transform.localPosition, farPos, Time.deltaTime * 3);
        }
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMover : MonoBehaviour {

    public float speed;
    public float limit;

    
    public int x, z;
    

    // Use this for initialization
    void Start () {
        
        x = 0;
        z = 1;
	}
	
	// Update is called once per frame
	void Update () {
        MovePlayer();
        
        //TurnAlt();
        this.transform.Translate(speed * x, 0, speed *z );

    }

    void MovePlayer()
    {
        
        if (this.transform.position.z < -limit || this.transform.position.x < -limit || this.transform.position.z > limit || this.transform.position.x > limit) 
        {
            
            x *= -1;
            z *= -1;
        }
        

    }

    //void Turn()
    //{
    //    if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
    //    {
    //        if(turned == false)
    //        {
    //            turned = true;
    //        }
    //        else if (turned == true)
    //        {
    //            turned = false;
    //        }
    //    }
    //    if(turned)
    //    {
    //        x = 1;
    //        z = 0;
    //    }
    //    if (!turned)
    //    {
    //        x = 0;
    //        z = 1;
    //    }
    //}

    void TurnAlt()
    {
        Vector2 mousePos = Input.mousePosition;
        if (Input.GetKeyDown(KeyCode.D) ||(Input.GetMouseButtonDown(0) && ( mousePos.x > Screen.width/2)))
        {
            if (x== 1 || x == -1)
            {
                x = 0;
                z = -1;
            }
            else if (z == 1 || z == -1)
            {
                x = 1;
                z = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.A) || (Input.GetMouseButtonDown(0) && (mousePos.x < Screen.width / 2)))
        {
            if (x == 1 || x == -1)
            {
                x = 0;
                z = 1;
            }
            else if (z == 1 || z == -1)
            {
                x = -1;
                z = 0;
            }
        }

    }
    void TurnAltNew()
    {
        Vector2 mousePos = Input.mousePosition;
        if ((Input.GetMouseButtonDown(0) && (mousePos.x > Screen.width / 2)))
        {
            if (mousePos.y > Screen.height / 2)
            {
                x = 1;
                z = 0;
            }
            else if (mousePos.y < Screen.height / 2)
            {
                x = 0;
                z = -1;
            }
        }
        if ((Input.GetMouseButtonDown(0) && (mousePos.x < Screen.width / 2)))
        {
            if (mousePos.y > Screen.height / 2)
            {
                x = 0;
                z = 1;
            }
            else if (mousePos.y < Screen.height / 2)
            {
                x = -1;
                z = 0;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "coin")
        {
            //Destroy(other.gameObject);
        }
    }
}

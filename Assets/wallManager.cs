﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallManager : MonoBehaviour {
    public float speed;
    public float wallIntervalDistance;
    public GameObject[] wall;
    public GameObject[] coinMaker;
    public GameObject starCoin;
    public GameObject gameManager;
    float wallCreatPointZ;
    public bool starCoinMaking = false;
    public bool finishedLevel = false;
    public float lastCoinPosition;
    public GameObject finishLine;
    public int maxWallType;

    public int currentLevel;

    int i;

    // Use this for initialization
    void Start () {

        wallCreatPointZ = transform.position.z + wallIntervalDistance;
        lastCoinPosition = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
        currentLevel = gameManager.GetComponent<gameManagerr>().level;

        if (gameManager.GetComponent<gameManagerr>().playing == true)
        {
            this.transform.Translate(0, 0, speed);
            

            if (transform.position.z - lastCoinPosition > 150f)
            {
                starCoinMaking = true;
                lastCoinPosition = transform.position.z;
            }

            if (transform.position.z > wallCreatPointZ && transform.position.z < 500f)
            {
                MakeWall();
                //MakeCoinMaker();
                if (starCoinMaking == true)
                {
                    starCoinMaker();
                }
                else if (starCoinMaking == false)
                {
                    MakeCoinMaker();
                }

                wallCreatPointZ += wallIntervalDistance;
            }
            if (transform.position.z > 520f && finishedLevel == false)
            {
                Instantiate(finishLine, transform.position, transform.rotation);
                finishedLevel = true;
            }
        }

        
    
    
    }

    void MakeWall()
    {
        if (currentLevel <= 15)
        {
            maxWallType = currentLevel + 1;
            i = Random.Range(0, maxWallType);
        }
        if (currentLevel > 15)
        {
            maxWallType = 20 + 1;
            i = Random.Range(0, maxWallType);
        }
        //if (currentLevel == 1)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 2)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 3)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 4)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 5)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 6)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 7)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 8)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 9)
        //{
        //    maxWallType = currentLevel + 1;
        //    i = Random.Range(0, maxWallType);
        //}
        //if (currentLevel == 10)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 11)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 12)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 13)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 14)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 15)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 16)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 17)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 18)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel == 19)
        //{
        //    i = Random.Range(0, wall.Length);
        //}
        //if (currentLevel >= 20)
        //{
        //    i = Random.Range(0, wall.Length);
        //}

        //int i = Random.Range(0, wall.Length);
        Instantiate(wall[i], transform.position, transform.rotation);
    }
    void MakeCoinMaker()
    {
        int i = Random.Range(0,coinMaker.Length);
        Instantiate(coinMaker[i], transform.position + new Vector3(0,0,8), transform.rotation);
    }
    void starCoinMaker()
    {
        float x = Random.Range(-8,8);
        Instantiate(starCoin, transform.position + new Vector3(x, 0, 8), transform.rotation);
        starCoinMaking = false;
    }
}

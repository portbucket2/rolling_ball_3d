﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, target.transform.position.z), Time.deltaTime *8f);
		
	}
}

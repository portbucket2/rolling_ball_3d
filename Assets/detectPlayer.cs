﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectPlayer : MonoBehaviour {

    public GameObject player;
    public ParticleSystem coinBreakParticle;
    public GameObject coin;
    public bool breakCoin;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
        breakCoin = false;
	}
	
	// Update is called once per frame
	void Update () {
		if( (player.transform.position.z- this.transform.position.z)> 1 && breakCoin == false)
        {
            coinBreakParticle.Play();
            coin.SetActive(false);
            breakCoin = true;
        }
	}
}

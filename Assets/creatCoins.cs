﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class creatCoins : MonoBehaviour {

    public GameObject coin;
    public int maxCoins;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < maxCoins; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-10,10),0, Random.Range(-10, 10));
            Instantiate(coin, pos, Quaternion.identity);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

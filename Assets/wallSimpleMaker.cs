﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallSimpleMaker : MonoBehaviour {

    public float posX;
    public float size;
    public GameObject wallHolder;
    public float maxSize;

	// Use this for initialization
	void Start () {
        size = Random.Range(5, maxSize);
        posX = Random.Range(-size/2, size/2);
        wallHolder.transform.localScale = new Vector3(size,1,1);
        transform.position = new Vector3(posX,0, transform.position.z);



    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameManagerr : MonoBehaviour
{

    public GameObject menuCanvas;
    public GameObject player;
    public GameObject playCanvas;
    public GameObject pauseCanvas;
    public GameObject levelEndCanvas;
    public GameObject nextLevelButton;
    public Text gameStarsText;
    public Text TotalStarsText;
    public Text coinsCurrentText;
    public Text coinsTotalText;
    public Text moreCoinsText;

    public Camera cam;

    public ParticleSystem[] sideWallParticle;

    public Color levelColor;
    public Color wallColor;

    public Color levelColor1;
    public Color levelColor2;
    public Color levelColor3;
    public Color levelColor4;
    public Color levelColor5;
    public Color levelColor6;
    public Color levelColor7;
    public Color levelColor8;
    public Color levelColor9;
    public Color levelColor10;

    public Material floorMat;
    public Material wallMat;

    

    public bool menu;
    public bool playing;
    public bool paused;
    public bool levelEnd;

    public bool levelPass;

    public bool levelUpdated;

    public int level =1;
    public int coinsTotal;
    public int coinsCurrent;
    public int starsCurrent;
    public int starsTotal;

    private int unlockedLevels;

    public Text levelText;
    public Text playMenuLevelText;
    //public int currentlevel;





    // Use this for initialization
    void Start()
    {
        //menu = true;
        Menu();
        level = PlayerPrefs.GetInt("Level", level);
        //coinsCurrent = PlayerPrefs.GetInt("coinsCurrent", coinsCurrent);
        //UpdateCoinsTotal();
        coinsTotal = PlayerPrefs.GetInt("coinsTotal", coinsTotal);
        starsTotal = PlayerPrefs.GetInt("starsTotal", starsTotal);
        levelText.text = "" + level;
        playMenuLevelText.text = "" + level;
        levelUpdated = false;
        UpdateLevelColor();
        starsCurrent = 0;

        
        //levelColor1 = new Color32(39,78,116,0);
        //floorMat.color = levelColor;
    }

    // Update is called once per frame
    void Update()
    {
        //UpdateCoinsCurrent();
        //if(menu == true)
        //{
        //
        //}
        //int CurrentLevel ;

    }

    public void Menu()
    {
        menuCanvas.SetActive(true);
        playCanvas.SetActive(false);
        pauseCanvas.SetActive(false);
        levelEndCanvas.SetActive(false);
        
        menu   = true  ;
        playing= false;
        paused = false;
        levelEnd = false;
    }
    public void Playing()
    {
        menuCanvas.SetActive(false);
        playCanvas.SetActive(true);
        pauseCanvas.SetActive(false);
        levelEndCanvas.SetActive(false);
        menu = false;
        playing = true;
        paused = false;
        levelEnd = false;
    }
    public void Paused()
    {
        menuCanvas.SetActive(false);
        playCanvas.SetActive(false);
        pauseCanvas.SetActive(true);
        levelEndCanvas.SetActive(false);
        menu = false;
        playing = false;
        paused = true;
        levelEnd = false;
    }
    public void LevelEnded()
    {
        menuCanvas.SetActive(false);
        playCanvas.SetActive(false);
        pauseCanvas.SetActive(false);
        levelEndCanvas.SetActive(true);
        menu = false;
        playing = false;
        paused = false;
        levelEnd = true;
    }

    public void NextLevel()
    {
        if(levelUpdated == false)
        {
            level += 1;
            UpdateLevelNumber();
            levelUpdated = true;
            player.GetComponent<playerRunner>().Restart();
        }
        
        
    }
    public void RetryLevel()
    {
        if (levelUpdated == false)
        {
            //level += 1;
            //UpdateLevelNumber();
            levelUpdated = true;
            player.GetComponent<playerRunner>().Restart();
        }

    }
    public void UpdateLevelNumber()
    {
        PlayerPrefs.SetInt("Level", level);
    }

    public void UpdateCoinsTotal()
    {
        //coinsTotal += coinsCurrent;
        coinsTotal += 1;
        coinsTotalText.text = "" + coinsTotal;
        PlayerPrefs.SetInt("coinsTotal", coinsTotal);
    }
    public void UpdateCoinsCurrent()
    {
        coinsCurrent = player.GetComponent<playerRunner>().coins;
        coinsCurrentText.text = "" + coinsCurrent;
        //PlayerPrefs.SetInt("coinsCurrent", coinsCurrent);
    }
    public void UpdateStarsCurrent()
    {
        starsCurrent += 1;
        //PlayerPrefs.SetInt("starsCurrent", starsCurrent);
        gameStarsText.text = "" + starsCurrent;
    }
    public void UpdateStarsTotal()
    {
        starsTotal += 1;
        PlayerPrefs.SetInt("starsTotal", starsTotal);
        TotalStarsText.text = "" + starsTotal;
    }

    public void ResetLevels()
    {
        level = 1;
        starsTotal = 0;
        coinsTotal = 0;
        PlayerPrefs.SetInt("coinsTotal", coinsTotal);
        PlayerPrefs.SetInt("starsTotal", starsTotal);
        UpdateLevelNumber();
        levelText.text = "" + level;
        playMenuLevelText.text = "" + level;
    }

    public void LevelPassing()
    {
        if(starsTotal > (level * 6/3))
        {
            levelPass = true;
            nextLevelButton.SetActive(true);
            moreCoinsText.text = "";
        }
        else
        {
            levelPass = false;
            nextLevelButton.SetActive(false);
            moreCoinsText.text = "MORE STARS NEEDED "+ ((level*2)- starsTotal);
        }

        
    }

    void UpdateLevelColor()
    {

        if (level == 1 || (level % 10) == 1)
        {
            levelColor = levelColor1;
        }
        if (level == 2 || (level % 10) == 2)
        {
            levelColor = levelColor2;
        }
        if (level == 3 || (level % 10) == 3)
        {
            levelColor = levelColor3;
        }
        if (level == 4 || (level % 10) == 4)
        {
            levelColor = levelColor4;
        }
        if (level == 5 || (level % 10) == 5)
        {
            levelColor = levelColor5;
        }
        if (level == 6 || (level % 10) == 6)
        {
            levelColor = levelColor6;
        }
        if (level == 7 || (level % 10) == 7)
        {
            levelColor = levelColor7;
        }
        if (level == 8 || (level % 10) == 8)
        {
            levelColor = levelColor8;
        }
        if (level == 9 || (level % 10) == 9)
        {
            levelColor = levelColor9;
        }
        if (level == 10 || level == 20)
        {
            levelColor = levelColor10;
        }

        floorMat.color = levelColor;
        wallColor = levelColor + new Color32(70,70,70,0);
        wallMat.color = wallColor;
        sideWallParticle[0].startColor = levelColor;
        sideWallParticle[1].startColor = levelColor;

        float H, S, V;

        //Color32 backColor = Color.RGBToHSV( new Color (1,1,1,1), out  H,out S,out V);

        cam.backgroundColor = levelColor - new Color32(120, 120, 120, 0);
    }
}

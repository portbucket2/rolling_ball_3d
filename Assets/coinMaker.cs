﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinMaker : MonoBehaviour {

    public GameObject coin;
    public Vector3 coinPos;
    public int f;

	// Use this for initialization
	void Start () {

        
        f = Random.Range(0, 2);
        if(f== 0)
        {
            VerticalForm();
        }
        else if (f == 1)
        {
            HorizontalForm();
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void VerticalForm()
    {
        int n = Random.Range(3, 5);
        float x = Random.Range(-8, 8);
        coinPos = new Vector3(x, transform.position.y, transform.position.z);
        for (int i = 0; i < n; i++)
        {
            Instantiate(coin, coinPos, Quaternion.identity);
            coinPos.z += 2f;

        }
    }
    void HorizontalForm()
    {
        int n = Random.Range(3, 5);
        float x = Random.Range(-8, 8);
        int horizonfac = 1;
        if(x >= 0)
        {
            horizonfac = -1;
        }
        else if (x < 0)
        {
            horizonfac = 1;
        }
        coinPos = new Vector3(x, transform.position.y, transform.position.z);
        for (int i = 0; i < n; i++)
        {
            Instantiate(coin, coinPos, Quaternion.identity);
            coinPos.x += horizonfac *2f;

        }
    }
}
